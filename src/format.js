/*
Accepts a string of HTML data.
Accepts an object with prettier settings.

Returns a string of HTML with formatted script blocks.
*/
const prettier = require('prettier')
const parse = require('./parse')

const SCRIPT_REGEXP = /(<script(?:\s.*)?>)([\S\s]*)/i
const STYLE_REGEXP = /(<style(?:\s.*)?>)([\S\s]*)/i

const prettierHTML = function(html, passed_options) {
  const options = Object.assign(
    {
      parser: 'babel'
    },
    passed_options
  )

  const nodes = parse(html, options)

  nodes.reduce((collection, node) => {
    const splitString = []

    let replace_regexp = SCRIPT_REGEXP
    if (options.parser === 'css') {
      replace_regexp = STYLE_REGEXP
    }

    html.slice(...node).replace(replace_regexp, (m, s, c) => {
      splitString.push(...[s, c])
      return m
    })

    let indent = 0
    if (options.preserveIndent) {
      indent = splitString[1].match(/^[\n]*?([ ]*)\b/)[1].length
    }

    const javascript = splitString[1]
    splitString[1] = prettier.format(javascript, options)

    if (options.preserveIndent) {
      splitString[1] = splitString[1].split('\n').reduce((collection, line) => {
        line =
          Array(indent)
            .fill(' ')
            .join('') + line

        collection += '\n' + line

        return collection
      }, '')
    }

    // Keep script tags with no content on the same line.
    const joinChar = (() => {
      if (splitString[1] !== '') {
        return '\n'
      } else {
        return ''
      }
    })()

    node.push(splitString.join(joinChar))
    collection.push(node)

    return collection
  }, [])

  const cloneStr = '' + html
  let newStr = '' + cloneStr
  let diff = 0
  nodes.forEach((node, idx) => {
    const openScript = node[0]
    const closeScript = node[1]
    const formatted = node[2]

    newStr = [
      newStr.substring(0, openScript + diff),
      formatted,
      newStr.substring(closeScript + diff)
    ].join('')

    diff = newStr.length - cloneStr.length
  })

  return newStr
}

module.exports = prettierHTML
