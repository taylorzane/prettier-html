/*
Accepts a string of HTML data.

Returns an object with start and end points of all script tags.
*/

const _chunk = require('lodash.chunk')

const SCRIPT_REGEXP = /<script(?:\s.*?)?>|<\/script>/gi
const STYLE_REGEXP = /<style(?:\s.*?)?>|<\/style>/gi

const parse = function(html, options = {}) {
  let regex = SCRIPT_REGEXP

  if (options.parser === 'css') {
    regex = STYLE_REGEXP
  }

  const indices = []
  let match
  while ((match = regex.exec(html))) {
    indices.push(match.index)
  }

  if (indices.length % 2 !== 0) {
    console.error('Invalid string!')
    return []
  }

  const groups = _chunk(indices, 2)

  return groups
}

module.exports = parse
