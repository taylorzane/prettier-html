const format = require('./format')

const prettierHTML = function(html, options = {}) {
  const output = format(html, options)

  return output
}

module.exports = prettierHTML
