const prettierHTML = require('..')
const chalk = require('chalk')

// Single Block
function Test1(done) {
  const source = [
    '<div>',
    'Test',
    '</div>',
    '',
    '<script>',
    'export default {',
    '  data () {',
    '    return {',
    '',
    '    }',
    '  },',
    '  methods:',
    '  {',
    '    test ( ) {',
    '',
    '    }, test2(a ,b) {',
    '',
    '    }',
    '  }',
    '}</script>'
  ].join('\n')

  const expected = [
    '<div>',
    'Test',
    '</div>',
    '',
    '<script>',
    'export default {',
    '  data() {',
    '    return {};',
    '  },',
    '  methods: {',
    '    test() {},',
    '    test2(a, b) {}',
    '  }',
    '};',
    '</script>'
  ].join('\n')

  const output = prettierHTML(source, {
    singleQuote: true
  })

  done(output === expected)
}

// Multiple Blocks
function Test2(done) {
  const source = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<script>',
    'export default {',
    '  data () {',
    '    return {',
    '',
    '    }',
    '  },',
    '  methods:',
    '  {',
    '    test ( ) {',
    '',
    '    }, test2(a ,b) {',
    '',
    '    }',
    '  }',
    '}</script>'
  ].join('\n')

  const expected = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<script>',
    'export default {',
    '  data() {',
    '    return {};',
    '  },',
    '  methods: {',
    '    test() {},',
    '    test2(a, b) {}',
    '  }',
    '};',
    '</script>'
  ].join('\n')

  const output = prettierHTML(source, {
    singleQuote: true
  })

  done(output === expected)
}

// Single Block (no-semi)
function Test3(done) {
  const source = [
    '<div>',
    'Test',
    '</div>',
    '',
    '<script>',
    'export default {',
    '  data () {',
    '    return {',
    '',
    '    }',
    '  },',
    '  methods:',
    '  {',
    '    test ( ) {',
    '',
    '    }, test2(a ,b) {',
    '',
    '    }',
    '  }',
    '}</script>'
  ].join('\n')

  const expected = [
    '<div>',
    'Test',
    '</div>',
    '',
    '<script>',
    'export default {',
    '  data() {',
    '    return {}',
    '  },',
    '  methods: {',
    '    test() {},',
    '    test2(a, b) {}',
    '  }',
    '}',
    '</script>'
  ].join('\n')

  const output = prettierHTML(source, {
    singleQuote: true,
    semi: false
  })

  done(output === expected)
}

// Multiple Blocks (no-semi)
function Test4(done) {
  const source = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<script>',
    'export default {',
    '  data () {',
    '    return {',
    '',
    '    }',
    '  },',
    '  methods:',
    '  {',
    '    test ( ) {',
    '',
    '    }, test2(a ,b) {',
    '',
    '    }',
    '  }',
    '}</script>'
  ].join('\n')

  const expected = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<script>',
    'export default {',
    '  data() {',
    '    return {}',
    '  },',
    '  methods: {',
    '    test() {},',
    '    test2(a, b) {}',
    '  }',
    '}',
    '</script>'
  ].join('\n')

  const output = prettierHTML(source, {
    singleQuote: true,
    semi: false
  })

  done(output === expected)
}

// CSS (scss)
function Test5(done) {
  const source = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<style type="text/scss">',
    '#id {',
    'color:cyan',
    ';',
    '}',
    '',
    '.class {background-color: blue;',
    '}',
    '',
    '.parent {',
    '.nested {',
    'color: blue;',
    '}}',
    '</style>'
  ].join('\n')

  const expected = [
    '<PascalCaseTest>',
    'Test',
    '</PascalCaseTest>',
    '',
    '<div :empty-attr></div>',
    '',
    '<style type="text/scss">',
    '#id {',
    '  color: cyan;',
    '}',
    '',
    '.class {',
    '  background-color: blue;',
    '}',
    '',
    '.parent {',
    '  .nested {',
    '    color: blue;',
    '  }',
    '}',
    '</style>'
  ].join('\n')

  const output = prettierHTML(source, {
    singleQuote: true,
    parser: 'css'
  })

  done(output === expected)
}

const tests = [
  ['Single Block (semi)', Test1],
  ['Multiple Blocks (semi)', Test2],
  ['Single Block (no-semi)', Test3],
  ['Multiple Blocks (no-semi)', Test4],
  ['CSS (scss)', Test5]
]

let success = 0
tests.forEach(test => {
  const name = test[0]
  const fn = test[1]
  fn(result => {
    success += +result
    console.log(`${result ? chalk.green('✔︎') : chalk.red('✘')}  ${name}`)
  })
})

const color = success === tests.length ? chalk.green : chalk.red
console.log(color(`[${success}/${tests.length}] passing`))

process.exit(success === tests.length ? 0 : 1)
