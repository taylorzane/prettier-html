import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'

export default {
  input: 'src/node.js',
  output: {
    file: 'dist/node.js',
    format: 'umd',
    name: 'prettierHTML'
  },
  external: ['prettier', 'jsdom'],
  plugins: [
    json(),
    resolve(),
    commonjs({
      ignore: ['prettier', 'jsdom']
    })
  ]
}
