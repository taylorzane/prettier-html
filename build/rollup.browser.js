import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'
import builtins from 'rollup-plugin-node-builtins'

export default {
  input: 'src/browser.js',
  output: {
    file: 'dist/browser.js',
    format: 'umd',
    name: 'prettierHTML'
  },
  plugins: [json(), builtins(), resolve(), commonjs()]
}
